package main

import (
	"fmt"
	"io/ioutil"
	"os"
)

func main() {
	f, err := os.Open("nombres.txt")
	if err != nil {
		fmt.Println(err)
		return
	}
	defer f.Close() // defer para que se cierre al final

	bs, err := ioutil.ReadAll(f)
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println(string(bs)) // James Bond
}
