package main

import (
	"fmt"
	"io"
	"os"
	"strings"
)

func main() {
	// retorna puntero a archivo y error
	//func Create(name string) (*File, error) {
	f, err := os.Create("nombres.txt")
	if err != nil {
		fmt.Println(err)
		return
	}
	defer f.Close()

	// func NewReader(s string) *Reader
	r := strings.NewReader("James Bond")

	io.Copy(f, r)
}
