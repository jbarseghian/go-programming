package main

import "fmt"

// 2 - Deferred function calls are executed in Last In First Out order after the surrounding function returns.
func main() {
	b()
}
func b() {
	for i := 0; i < 4; i++ {
		defer fmt.Print(i) //3210
	}
}
