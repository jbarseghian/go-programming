package main

import "fmt"

// 3 - Deferred functions may read and assign to the returning function’s named return values.
func main() {
	var x int
	x++
	fmt.Println(x)

	v := c()
	fmt.Println(v) // 2
}

func c() (i int) {
	defer func() { i++ }()
	return 1 // primero 1 entonce al defer func i vale 1 e incrementa y devuelve 2
}
