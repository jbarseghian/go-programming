package main

import "fmt"

// 1 - A deferred function’s arguments are evaluated when the defer statement is evaluated.
func main() {
	a()
}
func a() {
	i := 0
	defer fmt.Println(i) // print 0
	i++
	return
}
