package main

import (
	"fmt"
	"log"
	"os"
)

func main() {
	defer foo()
	_, err := os.Open("sin-archivo.txt")
	if err != nil {
		// Fatalln is equivalent to Println() followed by a call to os.Exit(1)
		log.Fatalln(err)
	}
}

func foo() {
	fmt.Println("Cuando os.Exit() es llamada, las funciones diferidas no corren")
}
