package main

import "fmt"

func main() {
	result := Abs(-10)
	fmt.Println(result)
}

func Abs(i int) int {
	if i < 0 {
		return i * -1
	} else {
		return i
	}
}
