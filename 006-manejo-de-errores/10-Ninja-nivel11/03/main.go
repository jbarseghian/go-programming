package main

import (
	"fmt"
)

// Ejercicio Práctico #3
// Crea un struct “errorPer” el cual implemente la interfaz builtin.error. Crea una función “foo” que tenga un valor de tipo error como parámetro.
// Crea un valor de tipo “errorPer” y pásalo a “foo”.
type errorPer struct {
	info string
}

func (ep errorPer) Error() string {
	return fmt.Sprintf("aquí esta el error: %v", ep.info)
}

func foo(e error) {
	// es un assertion e.(errorPer).info le digo al compilador que es de errorPer
	fmt.Println("foo corrió -", e, "\n", e.(errorPer).info)
}

func main() {
	e1 := errorPer{"Necesito dormir más"}
	foo(e1)
	//foo corrió - aquí esta el error: Necesito dormir más
	// Necesito dormir más
}
