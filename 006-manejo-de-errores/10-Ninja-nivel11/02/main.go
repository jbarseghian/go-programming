package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"log"
)

type persona struct {
	Nombre   string
	Apellido string
	Frases   []string
}

// Ejercicio Práctico #2
// Comienza con este código. Crea un mensaje de error personalizado usando “fmt.Errorf”.
func main() {
	p1 := persona{
		Nombre:   "James",
		Apellido: "Bond",
		Frases:   []string{"Shaken, not stirred", "¿Algún último deseo?", "Nunca digas nunca."},
	}

	bs, err := aJSON(p1)
	if err != nil {
		log.Fatalln(err)
		//fmt.Println(err)
		//return
	}

	fmt.Println(string(bs))
}

// aJSON también necesita retorna un error
func aJSON(a interface{}) ([]byte, error) {
	bs, err := json.Marshal(a)
	if err != nil {
		//return []byte{}, fmt.Errorf("Hubo un error en aJSON: %v", err)
		//	return []byte{}, errors.New("Hubo un error en aJSON")
		return []byte{}, errors.New(fmt.Sprintf("Hubo un error en aJSON: %v", err))
	}
	return bs, nil
}
