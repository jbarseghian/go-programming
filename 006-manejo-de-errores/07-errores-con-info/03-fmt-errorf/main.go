package main

import (
	"fmt"
	"log"
)

func main() {
	_, err := sqrt(-10.23)
	if err != nil {
		log.Fatalln(err)
	}
}

func sqrt(f float64) (float64, error) {
	if f < 0 {
		//2023/03/26 15:27:17 de matemática elemental: no hay raiz cuadrada real de un número negativo: -10.23
		return 0, fmt.Errorf("de matemática elemental: no hay raiz cuadrada real de un número negativo: %v", f)
	}
	return 42, nil
}
