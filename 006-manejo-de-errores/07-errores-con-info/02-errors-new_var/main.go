package main

import (
	"errors"
	"fmt"
	"log"
)

var ErrMat = errors.New("de matemática elemental: no hay raiz cuadrada real de un número negativo")

func main() {
	fmt.Printf("%T\n", ErrMat) //*errors.errorString // puntero al paquete errors tipo errorString y de tipo error ya que implementa la interfaz error
	_, err := sqrt(-10)
	if err != nil {
		log.Fatalln(err)
	}
}

func sqrt(f float64) (float64, error) {
	if f < 0 {
		return 0, ErrMat
	}
	return 42, nil
}
