package main

import (
	"fmt"
	"runtime"
	"sync"
)

func main() {
	fmt.Printf("Número de CPUS al inicio: %v\n", runtime.NumCPU())
	fmt.Printf("Número de Gorutinas al inicio: %v\n", runtime.NumGoroutine())
	contador := 0
	const gs = 100
	var wg sync.WaitGroup
	wg.Add(gs)

	for i := 0; i < gs; i++ {
		//Lanzo gorutinas
		go func() {
			v := contador
			runtime.Gosched() //sede el procesador
			v++
			contador = v
			wg.Done()
		}()
		fmt.Printf("Número de Gorutinas al medio: %v\n", runtime.NumGoroutine())
	}

	wg.Wait()

	fmt.Printf("Número de CPUS al final: %v\n", runtime.NumCPU())
	fmt.Printf("Número de Gorutinas al final: %v\n", runtime.NumGoroutine())
	fmt.Println(contador)
}
