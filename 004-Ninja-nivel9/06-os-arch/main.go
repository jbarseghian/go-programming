package main

import (
	"fmt"
	"runtime"
)

func main() {
	fmt.Printf("OS: %v\n", runtime.GOOS)
	fmt.Printf("Arquitectura: %v\n", runtime.GOARCH)
}
