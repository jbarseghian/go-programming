package main

import (
	"fmt"
	"runtime"
	"sync"
)

func main() {
	fmt.Printf("Número de CPUS al inicio: %v\n", runtime.NumCPU())
	fmt.Printf("Número de Gorutinas al inicio: %v\n", runtime.NumGoroutine())
	contador := 0
	const gs = 100
	var wg sync.WaitGroup
	wg.Add(gs)
	var m sync.Mutex

	for i := 0; i < gs; i++ {
		//Lanzo gorutinas
		go func() {
			m.Lock()
			v := contador
			v++
			contador = v
			fmt.Println(contador)
			m.Unlock()
			wg.Done()
		}()
		fmt.Printf("Número de Gorutinas al medio: %v\n", runtime.NumGoroutine())
	}

	wg.Wait()

	fmt.Printf("Número de CPUS al final: %v\n", runtime.NumCPU())
	fmt.Printf("Número de Gorutinas al final: %v\n", runtime.NumGoroutine())
	fmt.Println(contador)
}
