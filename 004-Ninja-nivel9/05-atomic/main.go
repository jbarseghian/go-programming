package main

import (
	"fmt"
	"runtime"
	"sync"
	"sync/atomic"
)

func main() {
	fmt.Printf("Número de CPUS al inicio: %v\n", runtime.NumCPU())
	fmt.Printf("Número de Gorutinas al inicio: %v\n", runtime.NumGoroutine())
	var wg sync.WaitGroup

	var contador int64
	const gs = 100
	wg.Add(gs)

	for i := 0; i < gs; i++ {
		//Lanzo gorutinas
		go func() {
			atomic.AddInt64(&contador, 1)
			fmt.Println(atomic.LoadInt64(&contador))
			wg.Done()
		}()
		fmt.Printf("Número de Gorutinas al medio: %v\n", runtime.NumGoroutine())
	}

	wg.Wait()

	fmt.Printf("Número de CPUS al final: %v\n", runtime.NumCPU())
	fmt.Printf("Número de Gorutinas al final: %v\n", runtime.NumGoroutine())
	fmt.Println(contador)
}
