package main

import (
	"fmt"
	"runtime"
)

func main() {
	fmt.Printf("SO: %v\nArquitectura: %v\n", runtime.GOOS, runtime.GOARCH)
}
