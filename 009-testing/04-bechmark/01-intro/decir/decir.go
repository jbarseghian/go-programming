// Package decir nos permite saludar
package decir

import "fmt"

// Saludar nos permite saludar a una persona
func Saludar(s string) string {
	return fmt.Sprint("Bienvenido querido ", s)
}
