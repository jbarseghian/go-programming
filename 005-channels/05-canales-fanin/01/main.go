package main

import (
	"fmt"
	"sync"
)

// Fan in
// Tomando valores de varios canales y colocándolos en un canal.
func main() {
	par := make(chan int)
	impar := make(chan int)
	fanin := make(chan int)

	go enviar(par, impar)

	go recibir(par, impar, fanin)

	//recepción de valores de fain // los imprimo
	for v := range fanin {
		fmt.Println(v)
	}

	fmt.Println("Finalizando.")
}

// send channel uso canal par impar para enviar los datos
func enviar(par, impar chan<- int) {
	for i := 0; i < 10; i++ {
		if i%2 == 0 {
			par <- i
		} else {
			impar <- i
		}
	}
	close(par)
	close(impar)
}

// receive channel par e impar reciben
// fain recibe lo de par e impar
// par inmpar es recibe only
// fanin es send only
func recibir(par, impar <-chan int, fanin chan<- int) {
	var wg sync.WaitGroup
	wg.Add(2)

	go func() {
		for v := range par {
			fanin <- v
		}
		wg.Done()
	}()

	go func() {
		for v := range impar {
			fanin <- v
		}
		wg.Done()
	}()

	wg.Wait()
	close(fanin)
}
