package main

import "fmt"

// Goi rutina principal que recibe el valor
func main() {
	//buffered channel (canal con bufer)
	ca := make(<-chan int, 2) //canal receive only

	//ca <- 42 //no anda porque es un canal received only
	//ca <- 43 //no anda porque es un canal received only

	fmt.Println(<-ca)
	fmt.Println(<-ca)
	fmt.Println("----------")
	fmt.Printf("%T\n", ca)
}
