package main

import "fmt"

// Goi rutina principal que recibe el valor
func main() {
	//buffered channel (canal con bufer)
	ca := make(chan int, 2) //cantidad de elementos que voy a dejar en el canal //tamaño que me mantiene valores

	ca <- 42 //le envió al canal el int 42
	ca <- 43 //le envió al canal el int 42

	fmt.Println(<-ca)
	fmt.Println(<-ca)
	fmt.Println("----------")
	fmt.Printf("%T\n", ca)
}
