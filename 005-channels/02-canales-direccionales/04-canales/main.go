package main

import (
	"fmt"
)

func main() {
	c := make(chan int)
	cr := make(<-chan int) // receive (sólo para recibir)
	cs := make(chan<- int) // send (sólo para enviar)

	fmt.Println("-----")
	fmt.Printf("c\t%T\n", c)
	fmt.Printf("cr\t%T\n", cr)
	fmt.Printf("cs\t%T\n", cs)

	// específico a general no asigna
	//c = cr
	//c = cs

	//general a específico si asigna
	cr = c
	cs = c

	//general a  específico convierte
	fmt.Println("-----")
	fmt.Printf("c\t%T\n", (<-chan int)(c))
	fmt.Printf("c\t%T\n", (chan<- int)(c))

	//específico a general NO convierte
	fmt.Println("-----")
	//fmt.Printf("c\t%T\n", (chan int)(cr))
	//fmt.Printf("c\t%T\n", (chan int)(cs))
}
