package main

import "fmt"

func main() {
	c := make(chan int)

	//enviar
	go enviar(c)

	//recibir
	recibir(c) //espera que haya un valor en el canal

	fmt.Println("Finalizando.")
}
func enviar(c chan<- int) {
	c <- 42

}

func recibir(c <-chan int) {
	fmt.Println(<-c)
}
