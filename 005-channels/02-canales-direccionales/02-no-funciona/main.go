package main

import "fmt"

// Goi rutina principal que recibe el valor
func main() {
	//buffered channel (canal con bufer)
	ca := make(chan<- int, 2) //canal send only

	ca <- 42 //le envió al canal el int 42
	ca <- 43 //le envió al canal el int 42

	//fmt.Println(<-ca) //no es valido porque es un canal que solo envia es decifr no recibe
	//fmt.Println(<-ca) //no es valido porque es un canal que solo envia es decifr no recibe
	fmt.Println("----------")
	fmt.Printf("%T\n", ca)
}
