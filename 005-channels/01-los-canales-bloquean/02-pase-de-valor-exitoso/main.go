package main

import "fmt"

// Goi rutina principal que recibe el valor
func main() {
	//unbuffered channel (canal sin bufer)
	ca := make(chan int) // un canal con dos gorutines una envía y otra recibe

	//GoRutina que envia el valor
	go func() {
		ca <- 42 //le envió al canal el int 42
	}()
	fmt.Println(<-ca) // en main voy a recibir ca // es otra rutina entonces al ser dos no bloquea y anda
}
