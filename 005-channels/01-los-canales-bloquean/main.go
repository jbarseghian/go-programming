package main

import "fmt"

// Goi rutina principal
func main() {
	//unbuffered channel (canal sin bufer)
	ca := make(chan int) // un canal con dos gorutines una envía y otra recibe
	ca <- 42             //le envió al canal el int 42 como es un canal unbuffered se va a bloquear porque no hay una forutina que recibe
	//fatal error: all goroutines are asleep - deadlock!
	fmt.Println(<-ca) // en mail voy a recibir ca
}
