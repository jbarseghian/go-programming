package main

import "fmt"

// Goi rutina principal que recibe el valor
func main() {
	//buffered channel (canal con bufer)
	ca := make(chan int, 1) //cantidad de elementos que voy a dejar en el canal //tamaño que me mantiene valores

	ca <- 42 //le envió al canal el int 42
	ca <- 43 //le mando otro valor al canal, se bloquea codigo porwue buiffer es de 1
	//fatal error: all goroutines are asleep - deadlock!

	fmt.Println(<-ca) // en main voy a recibir ca // es otra rutina entonces al ser dos no bloquea y anda
}
