package main

import "fmt"

// Ejercicio Práctico #6
// Escribe un programa que:
// Ponga 100 números en un canal
// Extraiga los números del canal y los imprima
func main() {
	c := make(chan int)
	go func() {
		for i := 0; i < 10; i++ {
			c <- i
		}
		close(c)
	}()

	for i := range c {
		fmt.Println(i)
	}
}
