package main

import (
	"fmt"
)

// fatal error: all goroutines are asleep - deadlock!
// Hay solo una go rutina que es el main
// recordar los canales bloquean!!!
// hay que usar metodos para que no se bloqueen usamos patron de concurrencia
func main1() {
	c := make(chan int)
	c <- 42 // al mandar esto se bloquea y muere el programa
	fmt.Println(<-c)
}

// Una forma de solucionar el problema anterior es usar una go rutina y mandamos el valor a c 42
func main() {
	c := make(chan int)
	go func() {
		c <- 42 // envió en valor al canal en la go rutina
	}()
	fmt.Println(<-c) // en el prinicpal recibo el valor
}

// Tampoco anda
func main2() {
	c := make(chan int)
	c <- 42 // al mandar esto se bloquea y muere el programa
	go func() {
		fmt.Println(<-c)
	}()
}
