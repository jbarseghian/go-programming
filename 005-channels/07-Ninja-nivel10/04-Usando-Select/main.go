// Comenzando con este código, extrae los valores del canal usando una declaración select

package main

import (
	"fmt"
)

func main() {
	salir := make(chan int) // canal bidireccional
	c := gen(salir)

	recibir(c, salir)

	fmt.Println("A punto de finalizar.")
}

func recibir(c <-chan int, salir <-chan int) {
	for {
		select {
		case v := <-c:
			fmt.Println(v)
		case <-salir: // si salir tiene un valir salgo
			return
		}
	}

}

func gen(salir chan<- int) <-chan int { // recibe un canal de send only y retorna canal de receive only
	c := make(chan int) // canal bidireccional

	go func() { // funcion literal
		for i := 0; i < 10; i++ {
			c <- i // recibe
		}
		salir <- 1 // envia
	}()

	return c
}
