package main

import (
	"fmt"
)

// Ejercicio a solucionar
//func main1() {
//	c := make(chan int)
//	c <- 42 // al mandar esto se bloquea y muere el programa
//	fmt.Println(<-c)
//}

func main() {
	c := make(chan int, 1) // buffer de 1 por lo que no necesito la go func
	c <- 42
	fmt.Println(<-c)
}
