package main

import "fmt"

/* hacer que funciones este codigo
func main() {
	cs := make(chan<- int)

	go func() {
		cs <- 42
	}()
	fmt.Println(<-cs)

	fmt.Printf("------\n")
	fmt.Printf("cs\t%T\n", cs)
}
*/

func main() {
	//	cs := make(chan<- int) // canal de tipo send only
	cs := make(chan int) // canal de tipo bidireccional

	go func() {
		cs <- 42 // puedo hacerlo porque es send only
	}()

	//fmt.Println(<-cs) // no puedo recibir en un canal que es solo para enviar
	fmt.Println(<-cs) // lo soluciono cambiando al canal de tipo bidireccional en el make

	fmt.Printf("------\n")
	fmt.Printf("cs\t%T\n", cs)
}
