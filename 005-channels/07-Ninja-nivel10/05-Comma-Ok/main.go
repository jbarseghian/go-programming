package main

import (
	"fmt"
)

// Demuestra el uso del idioma coma ok con este código.
func main() {
	c := make(chan int) // canal bidireccional

	go func() {
		c <- 42 // enviamos el valor 42 a c
	}()
	v, ok := <-c       // reciba el valor 42
	fmt.Println(v, ok) //42 true

	close(c) // cierro el canal

	v, ok = <-c        //v tiene 0 porque el canal se cerró, como no recibe nada queda ok en false
	fmt.Println(v, ok) // 0 false
}
