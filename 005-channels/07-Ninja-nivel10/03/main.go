package main

import (
	"fmt"
)

// Comenzando con este código, extrae los valores del canal usando un ciclo for range
func main() {
	c := gen() // canal receive only
	recibir(c)

	fmt.Println("A punto de finalizar.")
}

// https://www.youtube.com/watch?v=owNgUV25Qbc&list=PLSAQnrUqbx7sOdjJ5Zsq5FvvYtI8Kc-C5&index=171
func recibir(c <-chan int) { // recive un canal receive only
	for v := range c { // si no cierro el canal c en gen aca da deadlock // si no cierro al tener el range sigo queriendo extraer valores y no hay mas se bloquea
		fmt.Println(v)
	}
}

// retorna canal receive only
func gen() <-chan int {
	c := make(chan int) // cannal de valores enteros

	go func() {
		for i := 0; i < 10; i++ {
			c <- i
		}
		//fatal error: all goroutines are asleep - deadlock! // ya que luego en el recibir usamos RANGE para lo cual debe estar cerrado el canal c antes!!!
		close(c) // debo cerrarlo porque luego llama al range
	}()

	return c
}

//0
//1
//2
//3
//4
//5
//6
//7
//8
//9
//A punto de finalizar.
