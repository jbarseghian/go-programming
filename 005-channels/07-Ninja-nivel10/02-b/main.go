package main

import "fmt"

/*
func main() {
	cr := make(<-chan int)

	go func() {
		cr <- 42
	}()
	fmt.Println(<-cr)

	fmt.Printf("------\n")
	fmt.Printf("cr\t%T\n", cr)
}
*/

func main() {
	//cr := make(<-chan int) // receive only
	cr := make(chan int) // cambio el canal a bidireccional

	go func() {
		cr <- 42 // no se puede
	}()
	fmt.Println(<-cr)

	fmt.Printf("------\n")
	fmt.Printf("cr\t%T\n", cr)
}
