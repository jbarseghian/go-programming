package main

import (
	"context"
	"fmt"
)

func main() {
	// Devuelve un contexto
	ctx := context.Background()

	fmt.Println("context:\t\t", ctx)
	fmt.Println("error de context:\t", ctx.Err()) // Error de context
	fmt.Printf("tipo de context:\t%T\n", ctx)     // tipo de contexto //*context.emptyCtx puntero a un contexto vacio
	fmt.Println("----------")
}
