package main

// https://go.dev/play/p/WabMOiQnw3a

import (
	"context"
	"fmt"
)

func main() {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel() // cancela cuando hemos finalizado, es decir cuando finalice el main

	for n := range gen(ctx) { // range sobre un canal
		fmt.Println(n)
		if n == 5 {
			break // luego del break se ejecuta el cancel que hace entrar al select con done
		}
	}
}

// recibe el ctx y retorna un canal receive only de nros enteros
func gen(ctx context.Context) <-chan int {
	dst := make(chan int)
	n := 1
	go func() {
		for {
			select {
			case <-ctx.Done(): // despues que cancelemos
				return // retornando para que no se fuge la gorutina
			case dst <- n:
				n++
			}
		}
	}()
	return dst
}
