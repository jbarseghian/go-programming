package main

import (
	"context"
	"fmt"
)

func main() {
	ctx := context.Background()

	fmt.Println("context:\t\t", ctx)              //context.Background
	fmt.Println("error de context:\t", ctx.Err()) //nil
	fmt.Printf("tipo de context:\t%T\n", ctx)     //*context.emptyCtx
	fmt.Println("----------")

	//ctx, cancel --Z retorna el cancel
	ctx, cancel := context.WithCancel(ctx)

	fmt.Println("context:\t\t", ctx)                  //context.Background.WithCancel
	fmt.Println("error de context err:\t", ctx.Err()) //nil
	fmt.Printf("tipo de context:\t%T\n", ctx)         // *context.cancelCtx
	fmt.Println("cancel:\t\t\t", cancel)              // 0x10235a170
	fmt.Printf("tipo de cancel:\t\t%T\n", cancel)     // context.CancelFunc
	fmt.Println("----------")

	cancel() //funcion que cancela el proceso

	fmt.Println("context:\t\t", ctx)              //context.Background.WithCancel
	fmt.Println("error de context:\t", ctx.Err()) // context canceled
	fmt.Printf("tipo de context:\t%T\n", ctx)     // *context.cancelCtx
	fmt.Println("cancel:\t\t\t", cancel)          // 0x10235a170
	fmt.Printf("tipo de cancel:\t\t%T\n", cancel) // context.CancelFunc
	fmt.Println("----------")
}
