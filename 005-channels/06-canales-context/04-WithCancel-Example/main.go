package main

import (
	"context"
	"fmt"
	"runtime"
	"time"
)

func main() {
	//devuelve un ctx y una func cancel
	ctx, cancel := context.WithCancel(context.Background()) //pasamos el contexto background

	fmt.Println("chequeo de error 1:", ctx.Err())           // nil
	fmt.Println("num gorutinas 1:", runtime.NumGoroutine()) // 1 que es la rutina principal del main

	//lanzamos una go rutina
	go func() {
		n := 0
		for { // for infinito
			select { // es como switch pero refiere a operaciones send receive
			case <-ctx.Done(): // done va a tener un valor cuando llame a la func cancelar() (mas abajo)
				return
			default:
				n++
				time.Sleep(time.Millisecond * 200) // retraso de 200 miliseconds // se ejecuta 10 veces
				fmt.Println("Trabajando", n)
			}
		}
	}()

	time.Sleep(time.Second * 2)                             // le doy 2 seg
	fmt.Println("chequeo de error:", ctx.Err())             // nil porque no cancel()
	fmt.Println("num gorutinas 2:", runtime.NumGoroutine()) //  2 porque esta la main y el go func

	fmt.Println("A punto de cancelar context.")
	cancel()
	fmt.Println("context cancelado.")

	time.Sleep(time.Second * 2)
	fmt.Println("chequeo de error 3:", ctx.Err())           // context canceled
	fmt.Println("num gorutinas 3:", runtime.NumGoroutine()) // 1 sola go rutina
}
