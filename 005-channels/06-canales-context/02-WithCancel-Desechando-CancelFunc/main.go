package main

import (
	"context"
	"fmt"
)

func main() {
	ctx := context.Background()

	fmt.Println("context:\t", ctx)           // context.Background
	fmt.Println("context err:\t", ctx.Err()) // nil
	fmt.Printf("context type:\t%T\n", ctx)   // *context.emptyCtx
	fmt.Println("--------------------------------------------------")

	// ctx, _ esta ultima es la funcion de cancel que la estamos desechando
	ctx, _ = context.WithCancel(ctx) //func WithCancel(parent Context) (ctx Context, cancel CancelFunc)

	fmt.Println("context:\t", ctx)           //context.Background.WithCancel
	fmt.Println("context err:\t", ctx.Err()) //nil
	fmt.Printf("context type:\t%T\n", ctx)   //*context.cancelCtx
	fmt.Println("--------------------------------------------------")
}
