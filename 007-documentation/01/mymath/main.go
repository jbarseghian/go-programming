// Package mymath provides math solutions.
package mymath

// Sum suma un número ilimitado de valores de tipo int
func Sum(xi ...int) int {
	sum := 0

	for _, v := range xi {
		sum += v
	}

	return sum
}

// go doc mymath
// go doc mymath.Sum
// go doc text/template new
// go doc fmt Println
//go doc fmt.Println
